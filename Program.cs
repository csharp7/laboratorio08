﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Laboratorio8
{
    class Program
    {
        static void Main(string[] args)
        {
/*
            List<string> listaStrings = new List<string>();
            listaStrings.Add("um");
            listaStrings.Add("Hello");
            listaStrings.Add("World");
            Console.WriteLine(listaStrings[0]);
            Console.WriteLine(listaStrings[1]);
            Console.WriteLine(listaStrings[2]);
            
            Queue<Object> q = new Queue<object>();
            q.Enqueue(".Net Framework");
            q.Enqueue(new Decimal(123.456));
            q.Enqueue(654.321);
            Console.WriteLine(q.Dequeue());
            Console.WriteLine(q.Dequeue());
            Console.WriteLine(q.Dequeue());

            Queue<int> fila1 = new Queue<int>();
            fila1.Enqueue(10);
            fila1.Enqueue(100);
            fila1.Enqueue(1000);
            Console.WriteLine(fila1.Dequeue());
            Console.WriteLine(fila1.Dequeue());
            Console.WriteLine(fila1.Dequeue());
            
            Dictionary<int, string> paises = new Dictionary<int, string>();
            paises[44] = "Reino Unido";
            paises[33] = "França";
            paises[55] = "Brasil";
            Console.WriteLine("O código 55 é o {0}", paises[55]);
            foreach(KeyValuePair<int, string> item in paises)
            {
                int codigo = item.Key;
                string pais = item.Value;
                Console.WriteLine("Código {0} = {1}", codigo, pais);
            }

            string valor = "";
            paises.TryGetValue(33, out valor);
            Console.WriteLine("DDI {0} = {1}", "33", valor );
*/

            var rand = new Random();

            List<int> listaNum = new List<int>();

            for ( int i = 0; i < 10; i++)
            {
                listaNum.Add(rand.Next(0, 100));
            }

            static double TotalAcimaMedia(List<int> lista){
                double avg = lista.Average();
                int counter = 0;
                foreach( int i in lista)
                {
                    if (i > avg)
                    {
                        counter++;
                    } 
                }
                return counter;
            }
            
            Console.WriteLine($"{TotalAcimaMedia(listaNum)} números estão acima da média.");

            var ListaAcimaMedia = from i in listaNum where i > listaNum.Average() select i;
            foreach( int x in ListaAcimaMedia)
            {
                Console.WriteLine(x);
            }

        }
    }
}
